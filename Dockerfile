FROM debian:latest
RUN apt-get update 
RUN apt-get -y install build-essential libgmp10 libmpfr6 libmpc3 libgmp-dev libmpfr-dev libmpc-dev file texinfo bmake libncurses-dev vim
RUN mkdir $HOME/os161 && mkdir $HOME/os161/toolbuild && mkdir $HOME/os161/tools && mkdir $HOME/os161/tools/bin
RUN mkdir /scratch
WORKDIR /scratch
COPY files/binutils-2.24+os161-2.1.tar.gz /scratch/
RUN tar -xvzf binutils-2.24+os161-2.1.tar.gz
WORKDIR /scratch/binutils-2.24+os161-2.1
RUN find . -name '*.info' | xargs touch
RUN touch intl/plural.c
RUN ./configure --nfp --disable-werror --target=mips-harvard-os161 --prefix=$HOME/os161/tools
RUN make && make install 
WORKDIR /scratch
COPY files/gcc-4.8.3+os161-2.1.tar.gz /scratch/
COPY files/fix-attex-should-only-appear-at-a-line-beginning-warning.patch /scratch/ 
RUN tar -xvzf gcc-4.8.3+os161-2.1.tar.gz
WORKDIR /scratch/gcc-4.8.3+os161-2.1
RUN find . -name '*.info' | xargs touch
RUN touch intl/plural.c
WORKDIR /scratch/gcc-4.8.3+os161-2.1/gcc/doc
RUN patch -p0 < /scratch/fix-attex-should-only-appear-at-a-line-beginning-warning.patch
WORKDIR /scratch
RUN mkdir buildgcc
WORKDIR /scratch/buildgcc
RUN  ../gcc-4.8.3+os161-2.1/configure \
	--enable-languages=c,lto \
	--nfp --disable-shared --disable-threads \
	--disable-libmudflap --disable-libssp \
	--disable-libstdcxx --disable-nls \
	--target=mips-harvard-os161 \
	--prefix=$HOME/os161/tools
RUN gmake
RUN gmake install
WORKDIR /scratch
COPY files/gdb-7.8+os161-2.1.tar.gz /scratch/
RUN tar -xvzf gdb-7.8+os161-2.1.tar.gz
WORKDIR /scratch/gdb-7.8+os161-2.1/
RUN ./configure --target=mips-harvard-os161 --prefix=$HOME/os161/tools 
RUN make CFLAGS=-fgnu89-inline
RUN make install
WORKDIR /scratch
COPY files/sys161-2.0.8.tar.gz /scratch/
RUN tar -xvzf sys161-2.0.8.tar.gz
WORKDIR /scratch/sys161-2.0.8
RUN ./configure --prefix=$HOME/os161/tools mipseb
COPY files/sys161-multiple-definition.patch /scratch/sys161-2.0.8
RUN patch -p0 < sys161-multiple-definition.patch
RUN make
RUN make install
WORKDIR /scratch
COPY rename_script.sh /scratch/
RUN chmod +x /scratch/rename_script.sh
RUN /scratch/rename_script.sh
WORKDIR /scratch
COPY files/os161-base-2.0.3.tar.gz /scratch/
RUN tar -xvzf /scratch/os161-base-2.0.3.tar.gz
WORKDIR /scratch/os161-base-2.0.3/
ENV PATH=$PATH:/root/os161/tools/bin
RUN bmake
CMD /bin/bash
