# os161-docker

## Description

OS/161 (http://www.os161.org/) is an instructional operating system produced by
David A. Holland, Ada T. Lin, and Margo I. Seltzer for use in undergraduate and
graduate level operating systems courses.  It runs on top of System/161, a
MIPS-based virtual system based on the r3000 architecture.  This environment
can be somewhat difficult to set up and configure, because it has particular
dependenices, many of which don't build under modern compilers without
patching. 

This repository contains a set of files for constructing such an environment
inside a Docker container.  This environment is built following the
instructions at http://www.os161.org/resources/setup.html and provides a base,
vanilla environment with no customizations for particular offerings of the
course.

The System/161 tools are installed to the ~/os161/tools/bin PATH and the os161
sources are placed in ~/os161/src for the root user.

## Installation
To build the container, run the following from the os161-docker directory:

docker build -t os161 .

To run the container, do:

docker run --rm -it --name os161 os161

## Usage
Once you are in the container, you can follow the instructions at http://www.os161.org/resources/building.html to configure and compile a kernel.  

## License 
OS/161 and System/161 are copyrighted by the president and fellows of Harvard
University (see license.txt).  Feel free to use the Dockerfile and other scripts
under the public domain.

## Project status
I don't plan to actively maintain this repository, but patches are welcome and
I will try to integrate them as quickly as possible.  Feel free to submit issue
requests. 
